const MY_NOTION_TOKEN = PropertiesService.getScriptProperties().getProperty("NOTION_TOKEN");
const NOTION_VERSION = PropertiesService.getScriptProperties().getProperty("NOTION_VERSION");
const DATABASE_ID = PropertiesService.getScriptProperties().getProperty("DATABASE_ID_HAVIT");

function main() {
  doEverydayPost();
}

function doEverydayPost() {
  let d = new Date();
  let tomorrow = new Date(d.setDate(d.getDate() + 1));
  let date_str = Utilities.formatDate(tomorrow, "JST", "yyyy-MM-dd");
  let title_base = "YYYY/MM/dd";
  let title = Utilities.formatDate(tomorrow, "JST", title_base);

  postNotion(createPayload(title, date_str));
}

function postNotion(payload) {
  let url = "https://api.notion.com/v1/pages"; // API URL
  let options = {
    "method": "POST",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + MY_NOTION_TOKEN,
      "Notion-Version": NOTION_VERSION,
    },
    "payload": JSON.stringify(payload),
    "muteHttpExceptions": true
  };

  try {
    let res = UrlFetchApp.fetch(url, options);
    Logger.log(res)
  } catch (e) {
    Logger.log(e);
  }
}

function createPayload(title, date_str) {
  payload = {
    "parent": {
      "database_id": DATABASE_ID
    },
    "properties": {
      "Name": {
        "title": [
          {
            "text": {
              "content": title
            }
          }
        ]
      },
      "Date": {
        "date": {
          "start": date_str
        }
      }
    }
  }
  return payload;
}
